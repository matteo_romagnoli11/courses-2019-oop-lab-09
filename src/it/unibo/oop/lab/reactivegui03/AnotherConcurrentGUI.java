package it.unibo.oop.lab.reactivegui03;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public final class AnotherConcurrentGUI extends JFrame {
	
	private static final long serialVersionUID = 1L;
    private static final double WIDTH_PERC = 0.2;
    private static final double HEIGHT_PERC = 0.1;
    private final JLabel display = new JLabel();
    private final JButton up = new JButton("up");
    private final JButton down = new JButton("down");
    private final JButton stopGUI = new JButton("stop");
    /**
     *  Builds a ConcurrentGUI with three buttons.
     * 
     */
    public AnotherConcurrentGUI() {
    	super();
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize((int) (screenSize.getWidth() * WIDTH_PERC), (int) (screenSize.getHeight() * HEIGHT_PERC));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        final JPanel panel = new JPanel();
        panel.add(display);
        panel.add(up);
        panel.add(down);
        panel.add(stopGUI);
        this.getContentPane().add(panel);
        this.setVisible(true);
        /*
         * Create the counter agent and start it. This is actually not so good:
         * thread management should be left to
         * java.util.concurrent.ExecutorService
         */
        final Agent agent = new Agent();
        /*
         * Register a listener that stops it
         */
        stopGUI.addActionListener(new ActionListener() {
        	/**
             * event handler associated to action event on button stop.
             * 
             * @param e
             *            the action event that will be handled by this listener
             */
			@Override
			public void actionPerformed(final ActionEvent e) {
				agent.stopCounting();
	            up.setEnabled(false);
	            down.setEnabled(false);
	            stopGUI.setEnabled(false);
			}
        });
        /*
         * Register a listener that invert the counter from backward to forward
         */
        up.addActionListener(e -> agent.changeDirection());
        /*
         * Register a listener that invert the counter from forward to backward
         */
        down.addActionListener((e) -> agent.changeDirection());
        new Thread(agent).start();
        agent.agentStopper();
    }

    private class Agent implements Runnable {
        /*
         * Stop is volatile to ensure visibility. Look at:
         * 
         * http://archive.is/9PU5N - Sections 17.3 and 17.4
         * 
         * For more details on how to use volatile:
         * 
         * http://archive.is/4lsKW
         * 
         */
        private volatile boolean stop;
        private volatile int counter;
        private volatile boolean direction = true;

        @Override
        public void run() {
            while (!this.stop) {
                try {
                    /*
                     * All the operations on the GUI must be performed by the
                     * Event-Dispatch Thread (EDT)!
                     */
                    SwingUtilities.invokeAndWait(() -> AnotherConcurrentGUI.this.display.setText(Integer.toString(Agent.this.counter)));
                    this.counter += this.direction ? 1 : -1;
                    Thread.sleep(100);
                } catch (InvocationTargetException | InterruptedException ex) {
                    /*
                     * This is just a stack trace print, in a real program there
                     * should be some logging and decent error reporting
                     */
                    ex.printStackTrace();
                }
            }
        }
        /**
         * External command to stop counting.
         */
        public void stopCounting() {
            this.stop = true;
        }
        public void changeDirection() {
        	this.direction = !this.direction;
        }
        public void agentStopper() {
        	try {
	        	Thread.sleep(10_000);
	        	this.stop = true;
	        	up.setEnabled(false);
	        	down.setEnabled(false);
	        	stopGUI.setEnabled(false);
        	} catch (Exception ex) {
        		ex.printStackTrace();
        	}
        }
    }
}
