package it.unibo.oop.lab.workers02;

import java.util.ArrayList;
import java.util.List;

public class MultiThreadedSumMatrix implements SumMatrix{
	
	private int nthread;
	
	public MultiThreadedSumMatrix(final int threads) {
		this.nthread = threads;
	}
	
	private static class Worker extends Thread {
        private final double[][] matrix;
        private final int startrow;
        private final int rows;
        private long res;

        /**
         * Build a new worker.
         * 
         * @param list
         *            the list to sum
         * @param startrow
         *            the initial position for this worker
         * @param rows
         *            the no. of elems to sum up for this worker
         */
        Worker(final double[][] matrix, final int startrow, final int rows) {
            super();
            this.matrix = matrix;
            this.startrow = startrow;
            this.rows = rows;
        }

        @Override
        public void run() {
            System.out.println("Working from position " + startrow + " to position " + (startrow + rows - 1));
            for (int i = startrow; i < matrix.length && i < startrow + rows; i++) {
            	for (int j = 0; j < matrix[i].length; j++) {
            		this.res += this.matrix[i][j];
            	}
            }
        }

        /**
         * Returns the risult of summing up the integers within the list.
         * 
         * @return the sum of every element in the array
         */
        public long getResult() {
            return this.res;
        }
	}
	public double sum(final double[][] matrix) {
		final int size = matrix.length % nthread + matrix.length / nthread;
        /*
         * Build a list of workers
         */
        final List<Worker> workers = new ArrayList<>(nthread);
        for (int start = 0; start < matrix.length; start += size) {
            workers.add(new Worker(matrix, start, size));
        }
        /*
         * Start them
         */
        for (final Worker w: workers) {
            w.start();
        }
        /*
         * Wait for every one of them to finish. This operation is _way_ better done by
         * using barriers and latches, and the whole operation would be better done with
         * futures.
         */
        long sum = 0;
        for (final Worker w: workers) {
            try {
                w.join();
                sum += w.getResult();
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
        }
        /*
         * Return the sum
         */
		return sum;
	}
}
